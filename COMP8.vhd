library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.all;

entity CMP8 is Port (
	clk : in STD_LOGIC;
	A,B : in STD_LOGIC_VECTOR(7 downto 0);
	sel : in STD_LOGIC_VECTOR(1 downto 0); --0 for EQ, 1 for LT, 2 for LEQ
	O: out STD_LOGIC_VECTOR(7 downto 0));
end CMP8;
architecture Behavioral of CMP8 is

begin
	process(clk) is begin
		case(sel) is 
			"00" => if signed(A) = signed(B) then
					O <= x"01";
				else O <= x"00";
			end if;
			"01" => if signed(A) < signed(B) then
					O <= x"01";
				else O <= x"00";
			end if;
			"10" => if signed(A) <= signed(B) then
					O <= x"01";
				else O <= x"00";
			end if;
			when others => O <= x"00";
		end case;
	end process;
end Behavioral;
