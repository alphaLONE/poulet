library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.all;

entity ADD8 is Port (
	clk : in STD_LOGIC;
	A,B : in STD_LOGIC_VECTOR(7 downto 0);
	sum: out STD_LOGIC_VECTOR(7 downto 0);
	overf : out STD_LOGIC
	);
end ADD8;
architecture Behavioral of ADD8 is
	signal sumPrime : signed(8 downto 0);
begin
	process(clk) is begin
		sumPrime <= signed(A) + signed(B);
		sum <= sumPrime(7 downto 0);
		overf <= sumPrime(8); 
	end process;
end Behavioral;