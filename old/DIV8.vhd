library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.all;

entity DIV8 is Port (
	clk : in STD_LOGIC;
	A,B : in STD_LOGIC_VECTOR(7 downto 0);
	qu: out STD_LOGIC_VECTOR(7 downto 0);
	rema : out STD_LOGIC_VECTOR(7 downto 0));
end DIV8;
architecture Behavioral of DIV8 is
	signal quPrime : STD_LOGIC_VECTOR(7 downto 0);
begin
	process(clk) is begin
		quPrime <= signed(A) / signed(B);
		rema <= signed(A) - (quPrime * signed(B));
		qu <= quPrime;
	end process;
end Behavioral;