library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.NUMERIC_STD.all;

entity ALU4 is Port (
	A,B : in STD_LOGIC_VECTOR(0 to 3);
	clk : in STD_LOGIC;
	ldA,ldB : in STD_LOGIC; -- enables write to input registers
	bcast : in STD_LOGIC; -- enables output to bus
	sel : in STD_LOGIC_VECTOR(0 to 2);
	flags : out STD_LOGIC_VECTOR(0 to 1); -- 0: carry, 1: null out
	O : out STD_LOGIC_VECTOR(0 to 3)
	);
end ALU4;
architecture Behavioral of ALU NUMERIC_STD
	signal regAout,regBout : STD_LOGIC_VECTOR(0 to 3);
	signal aluOut : STD_LOGIC_VECTOR(0 to 3);
	signal addOut: STD_LOGIC_VECTOR(0 to 4);
	signal flagsTmp: STD_LOGIC_VECTOR(0 to 1);

begin
	process(clk,ldA,ldB)
	begin
		if rising_edge(clk) then
			if ldA = '1' then
				regAout <= A;
			end if;
			if ldB = '1' then
				regBout <= B;
			end if;
		end if;
	end process;

	process(clk,sel)
	begin
		case(sel) is 
			when "000" => aluOut <= regAout;
			when "001" => aluOut <= regAout AND regBout;
			when "010" => aluOut <= regAout OR regBout;
			when "011" =>
				addOut <= regAout + regBout;
				aluOut <= addOut(0 to 3);
				flagsTmp(0) <= addOut(4);
			when "100" => aluOut <= not regAout;
			when "101" => if (regAout = regBout) then
					aluOut <= "0001";
				else aluOut <= "0000";
				end if;
			when "110" => if (regAout < regBout) then
					aluOut <= "0001";
				else aluOut <= "0000";
				end if;
			when "111" => aluOut <= shift_left(unsigned(regAout),unsigned(regBout));
		end case;
		if aluOut = "0000" then
			flagsTmp(1) <= '1';
		end if;
	end process;

	process(clk, bcast)
	begin
		if rising_edge(clk) then
			O <= aluOut;
			flags <= flagsTmp;
		end if;
	end process;
end Behavioral;
