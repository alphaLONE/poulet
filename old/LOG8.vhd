library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.all;

entity LOG8 is Port (
	clk : in STD_LOGIC;
	A,B : in STD_LOGIC_VECTOR(7 downto 0);
	sel : in STD_LOGIC_VECTOR(1 downto 0); --0 for AND,1 for OR, 2 for XOR, 3 for NOT(A)
	O: out STD_LOGIC_VECTOR(7 downto 0));
end LOG8;
architecture Behavioral of LOG8 is begin
	process(clk) is begin
		case(sel) is 
			when "00" => O <= A AND B;
			when "01" => O <= A OR B;
			when "10" => O <= A XOR B;
			when "11" => O <= NOT A;
			when others => O <= "--------";
		end case;
	end process;
end Behavioral;