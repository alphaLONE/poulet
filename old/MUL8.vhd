library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.all;

entity MUL8 is Port (
	clk : in STD_LOGIC;
	A,B : in STD_LOGIC_VECTOR(7 downto 0);
	prod: out STD_LOGIC_VECTOR(7 downto 0);
	overf : out STD_LOGIC
	);
end MUL8;

architecture Behavioral of MUL8 is
	signal prodPrime : STD_LOGIC_VECTOR(15 downto 0);
begin
	process(clk) is begin
		prodPrime <= signed(A)*signed(B);
		overf <= xor prodPrime(16 downto 8); --reduction allowed by VHDL2008
		prod <= prodPrime(7 downto 0);
	end process;
end Behavioral;