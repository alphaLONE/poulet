library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.all;

entity SHF8 is Port (
	clk : in STD_LOGIC;
	A : in STD_LOGIC_VECTOR(7 downto 0);
	B : in STD_LOGIC_VECTOR(2 downto 0);
	sel : in STD_LOGIC_VECTOR(1 downto 0); --0 for LEFT,1 for ARITH RIGHT, 2 for LOG RIGHT
	O: out STD_LOGIC_VECTOR(7 downto 0));
end SHF8;
architecture Behavioral of SHF8 is

begin
	process(clk) is begin
		case(sel) is
			"00" => O <= shift_left(A,B);
			"10" => O <= shift_right(signed(A),B);
			"11" => O <= shift_right(unsigned(A),B);
			when others => O <= "--------";
		end case;
	end process;
end Behavioral;

