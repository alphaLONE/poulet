LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY tb_ALU4 IS
END tb_ALU4;
 
ARCHITECTURE behavior OF tb_ALU4 IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ALU4
    PORT(
        A,B : in STD_LOGIC_VECTOR(0 to 3);
        clk : in STD_LOGIC;
        ldA,ldB : in STD_LOGIC; -- enables write to input registers
        bcast : in STD_LOGIC; -- enables output to bus
        sel : in STD_LOGIC_VECTOR(0 to 2);
        flags : out STD_LOGIC_VECTOR(0 to 1); -- 0: carry, 1: null out
        O : out STD_LOGIC_VECTOR(0 to 3)
        );
    END COMPONENT;
       --Inputs
   signal A : std_logic_vector(3 downto 0) := (others => '0');
   signal B : std_logic_vector(3 downto 0) := (others => '0');
   signal sel : std_logic_vector(2 downto 0) := (others => '0');
   signal bcast : in STD_LOGIC := '0';
   signal clk : in STD_LOGIC := '0';
   signal ldA : in STD_LOGIC := '0';
   signal ldB : in STD_LOGIC := '0';

  --Outputs
   signal O : std_logic_vector(3 downto 0);
   signal flags : std_logic_vector(1 downto 0);

   signal finished : std_logic := '0';
 
BEGIN
 
 -- Instantiate the Unit Under Test (UUT)
   uut: ALU PORT MAP (
          A => A,
          B => B,
          clk => clk,
          sel => sel,
          bcast => bcast,
          flags => flags,
          O => O,
          ldA => ldA,
          ldB => ldB
        );

 

   -- Stimulus process
   stim_proc: process
   begin 
    clk <= not clk after 0.5 ns when finished /= '1' else '0';
      -- hold reset state for 100 ns.
    A <= x"A";
    B <= x"2";
  ALU_Sel <= "001";
  
  for i in 0 to 15 loop 
   ALU_Sel <= ALU_Sel + x"1";
   wait for 100 ns;
  end loop;
    A <= x"F6";
    B <= x"0A";
      wait;
   end process;

END;