library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity MUL8 is Port (
	clk : in STD_LOGIC;
	A,B : in STD_LOGIC_VECTOR(7 downto 0);
	prod: out STD_LOGIC_VECTOR(7 downto 0);
	overf : out STD_LOGIC
	);
end MUL8;

architecture Behavioral of MUL8 is
	signal prodPrime : signed(15 downto 0);
	signal prodPrimePrime : STD_LOGIC_VECTOR(15 downto 0);
begin

		prodPrime <= signed(A) * signed(B);
		overf <= xor prodPrime(15 downto 8); --reduction allowed by VHDL2008
		prodPrimePrime <= STD_LOGIC_VECTOR(prodPrime);
		prod <= prodPrimePrime(7 downto 0);

end Behavioral;