library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity DIV8 is Port (
	clk : in STD_LOGIC;
	A,B : in STD_LOGIC_VECTOR(7 downto 0);
	qu: out STD_LOGIC_VECTOR(7 downto 0);
	rema : out STD_LOGIC_VECTOR(7 downto 0));
end DIV8;
architecture Behavioral of DIV8 is
	signal quPrime : signed(7 downto 0);
begin
	process(clk) is
	variable remaPrime : STD_LOGIC_VECTOR(15 downto 0);
	begin
		quPrime <= signed(A) / signed(B);
		remaPrime := STD_LOGIC_VECTOR(signed(A) - (quPrime * signed(B)));
		rema <= remaPrime(7 downto 0);
		qu <= STD_LOGIC_VECTOR(quPrime);
	end process;
end Behavioral;