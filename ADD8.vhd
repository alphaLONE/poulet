library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity ADD8 is Port (
	clk : in STD_LOGIC;
	A,B : in STD_LOGIC_VECTOR(7 downto 0);
	sum: out STD_LOGIC_VECTOR(7 downto 0);
	overf : out STD_LOGIC
	);
end ADD8;
architecture Behavioral of ADD8 is
    signal extendedA, extendedB : signed(8 downto 0);
	signal sumPrime : signed(8 downto 0);
	signal var : STD_LOGIC_VECTOR(8 downto 0);
begin
	    extendedA <= resize(signed(A),9);
	    extendedB <= resize(signed(B),9);
		sumPrime <= extendedA + extendedB;
		var <= STD_LOGIC_VECTOR(sumPrime);
		sum <= var(7 downto 0);
		overf <= var(8); 
end Behavioral;