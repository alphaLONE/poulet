library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.all;

entity ALU8 is Port (
	clk : in STD_LOGIC;
	A,B : in STD_LOGIC_VECTOR(7 downto 0);
	sel : in STD_LOGIC_VECTOR(4 downto 0);
	O : out STD_LOGIC_VECTOR(7 downto 0);
	flags : out STD_LOGIC_VECTOR(1 downto 0) --bit 0 is null result, 1 is overflow/remainder
	);
end ALU8;
architecture Behavioral of ALU8 is
	component ADD8 is Port (
	clk : in STD_LOGIC;
	A,B : in STD_LOGIC_VECTOR(7 downto 0);
	sum: out STD_LOGIC_VECTOR(7 downto 0);
	overf : out STD_LOGIC);
	end component;
	component MUL8 is Port (
	clk : in STD_LOGIC;
	A,B : in STD_LOGIC_VECTOR(7 downto 0);
	prod: out STD_LOGIC_VECTOR(7 downto 0);
	overf : out STD_LOGIC);
	end component;
	component DIV8 is Port (
	clk : in STD_LOGIC;
	A,B : in STD_LOGIC_VECTOR(7 downto 0);
	qu: out STD_LOGIC_VECTOR(7 downto 0);
	rema : out STD_LOGIC_VECTOR(7 downto 0));
	end component;
	component LOG8 is Port (
	clk : in STD_LOGIC;
	A,B : in STD_LOGIC_VECTOR(7 downto 0);
	sel : in STD_LOGIC_VECTOR(1 downto 0);
	O: out STD_LOGIC_VECTOR(7 downto 0));
	end component;
	component CMP8 is Port (
	clk : in STD_LOGIC;
	A,B : in STD_LOGIC_VECTOR(7 downto 0);
	sel : in STD_LOGIC_VECTOR(1 downto 0); --0 for EQ, 1 for LT, 2 for LEQ
	O: out STD_LOGIC_VECTOR(7 downto 0));
	end component;
	component SHF8 is Port (
	clk : in STD_LOGIC;
	A : in STD_LOGIC_VECTOR(7 downto 0);
	B : in STD_LOGIC_VECTOR(2 downto 0);
	sel : in STD_LOGIC_VECTOR(1 downto 0); --0 for LEFT,1 for ARITH RIGHT, 2 for LOG RIGHT
	O: out STD_LOGIC_VECTOR(7 downto 0));
	end component;

	signal clkAdd,clkMul,clkDiv,clkLog,clkCmp,clkShf: STD_LOGIC;
	signal logSel,cmpSel,shfSel : STD_LOGIC_VECTOR(1 downto 0);
	signal bNeg : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
	signal outPrime,addOut,mulOut,divOutQuotient,divOutRemainder,logOut,cmpOut,shfOut: STD_LOGIC_VECTOR(7 downto 0);
	signal addOverf,mulOverf : STD_LOGIC;
begin


	addBlock : ADD8 port map(clk=>clk,A=>A,B=>bNeg,sum=>addOut,overf=>addOverf);
	mulBlock : MUL8 port map(clk=>clk,A=>A,B=>bNeg,prod=>mulOut,overf=>mulOverf);
	divBlock : DIV8 port map(clk=>clkDiv,A=>A,B=>bNeg,qu=>divOutQuotient,rema=>divOutRemainder);
	logBlock : LOG8 port map(clk=>clk,A=>A,B=>bNeg,sel=>logSel,O=>logOut);
	cmpBlock : CMP8 port map(clk=>clk,A=>A,B=>bNeg,sel=>cmpSel,O=>cmpOut);
	shfBlock : SHF8 port map(clk=>clk,A=>A,B=>bNeg(2 downto 0),sel=>shfSel,O=>shfOut);
	process(clk) is
		variable bVal: signed(7 downto 0);
		variable outStat : std_logic;
	begin
		if rising_edge(clk) then
			case(sel(3 downto 0)) is 
				when "0000" => --ASKS FOR ADDITION
					clkAdd <= '0';
					if sel(4) = '1' then
						bVal := -signed(B);
					else
						bVal := signed(B);
					end if;
					bNeg <= STD_LOGIC_VECTOR(bVal);
					clkAdd <= '1';
					O <= addOut;
					outStat := or addOut;
					if outStat = '0' then
						flags(0) <= '1';
					else
						flags(0) <= '0';
					end if;
					flags(1) <= addOverf;
				when "0001" => --ASKS FOR MULTIPLICATION
					clkMul <= clk;
					if sel(4) = '1' then
						bVal := -signed(B);
					else
						bVal := signed(B);
					end if;
					bNeg <= STD_LOGIC_VECTOR(bVal);
					O <= mulOut;
					outStat := or mulOut;
					if outStat = '0' then
						flags(0) <= '1';
					else
						flags(0) <= '0';
					end if;
					flags(1) <= mulOverf;
				when "0010" => --ASKS FOR QUOTIENT
					clkDiv <= clk;
					if sel(4) = '1' then
						bVal := -signed(B);
					else
						bVal := signed(B);
					end if;
					bNeg <= STD_LOGIC_VECTOR(bVal);
					O <= divOutQuotient;
					outStat := or divOutQuotient;
					if outStat = '0' then
						flags(0) <= '1';
					else
						flags(0) <= '0';
					end if;
					flags(1) <= or divOutRemainder;
				when "0011" => --ASKS FOR REMAINDER OF DIVISION
					clkDiv <= clk;
					if sel(4) = '1' then
						bVal := -signed(B);
					else
						bVal := signed(B);
					end if;
					bNeg <= STD_LOGIC_VECTOR(bVal);
					O <= divOutRemainder;
					outStat := or divOutRemainder;
					if outStat = '0' then
						flags(0) <= '1';
					else
						flags(0) <= '0';
					end if;
					flags(1) <= '-';
				when "0100" | "0101" | "0110" | "0111" => --ASKS FOR LOGIC OPERATION, BITS ARE PASSED THROUGH TO SEL
					clkLog <= clk;
					if sel(4) = '1' then
						bVal := -signed(B);
					else
						bVal := signed(B);
					end if;
					logSel <= sel(1 downto 0);
					bNeg <= STD_LOGIC_VECTOR(bVal);
					O <= logOut;
					outStat := or logOut;
					if outStat = '0' then
						flags(0) <= '1';
					else
						flags(0) <= '0';
					end if;
					flags(1) <= '-';
				when "1000" | "1001" | "1010" | "1011" => --ASKS FOR COMPARISON, BITS ARE PASSED THROUGH TO SEL
					clkCmp <= clk;
					if sel(4) = '1' then
						bVal := -signed(B);
					else
						bVal := signed(B);
					end if;
					cmpSel <= sel(1 downto 0);
					bNeg <= STD_LOGIC_VECTOR(bVal);
					O <= cmpOut;
					outStat := or cmpOut;
					if outStat = '0' then
						flags(0) <= '1';
					else
						flags(0) <= '0';
					end if;
					flags(1) <= '-';
				when "1100" | "1101" | "1110" | "1111" => --ASKS FOR SHIFT, BITS ARE PASSED THROUGH SEL
					clkShf <= clk;
					if sel(4) = '1' then
						bVal := -signed(B);
					else
						bVal := signed(B);
					end if;
					shfSel <= "00";
					bNeg <= STD_LOGIC_VECTOR(bVal);
					O <= shfOut;
					outStat := or shfOut;
					if outStat = '0' then
						flags(0) <= '1';
					else
						flags(0) <= '0';
					end if;
					flags(1) <= '-';
				when others => O <= A;
			end case;
		end if;
	end process;
end Behavioral;

